package bigdata.java.platform.service;

import bigdata.java.platform.beans.TaskLog;
import bigdata.java.platform.beans.livy.StartResoult;

import java.util.Date;
import java.util.List;

public interface TaskLogService {
    TaskLog get(Integer taskLogId);
    void add(TaskLog taskLog);
    void edit(TaskLog taskLog);
    TaskLog addTaskId(Integer taskId, Date updateTime);
    Boolean deleteByTaskId(Integer taskId);
    Boolean updateBatchId(Integer taskId,Integer taskLogId,Integer batchId);
    Boolean addBatchId(Integer taskId,Integer batchId, Date updateTime);
    Boolean updateAppId(Integer taskId, Integer taskLogId, Integer batchId, StartResoult startResoult);
    List<TaskLog> listForGroupMaxId();
    List<TaskLog> list(Integer taskId);
    Boolean stop(Integer taskId,Integer taskLogId,Integer batchId);
    Boolean updateMonitor(Integer taskLogId,Integer waitingBatches,Integer totalCompletedBatches,Long totalProcessedRecords);
}
