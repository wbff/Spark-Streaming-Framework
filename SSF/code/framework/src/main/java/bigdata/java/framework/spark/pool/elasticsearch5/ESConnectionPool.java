/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.elasticsearch5;

import bigdata.java.framework.spark.pool.ConnectionPool;
import bigdata.java.framework.spark.pool.ConnectionPoolBase;
import org.elasticsearch.client.transport.TransportClient;

public class ESConnectionPool extends ConnectionPoolBase<TransportClient> implements ConnectionPool<TransportClient> {
    /**
     * <p>Title: ESConnectionPool</p>
     * <p>Description: 构造方法</p>
     *
     * @param esConfig  配置
     *
     */
    public ESConnectionPool(ESConfig esConfig) {
        super(esConfig, new ESConnectionFactory(esConfig));
    }

    @Override
    public TransportClient getConnection() {
        return super.getResource();
    }

    @Override
    public void returnConnection(TransportClient conn) {
        super.returnResource(conn);
    }

    @Override
    public void invalidateConnection(TransportClient conn) {
        super.invalidateResource(conn);
    }
}
