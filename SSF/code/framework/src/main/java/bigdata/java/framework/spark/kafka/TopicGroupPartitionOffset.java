/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import java.io.Serializable;

/**
 * kafka 主题，消费者组，分区，偏移量实体类
 */
public class TopicGroupPartitionOffset implements Serializable
{

    public String group;

    public String topic;

    public Integer partition;


    public Long earliestOffset=0L;

    public Long lastestOffset;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public Long getLastestOffset() {
        return lastestOffset;
    }

    public void setLastestOffset(Long lastestOffset) {
        this.lastestOffset = lastestOffset;
    }

    @Override
    public String toString() {
        return "TopicGroupPartitionOffset{" +
                "group='" + group + '\'' +
                ", topic='" + topic + '\'' +
                ", partition=" + partition +
                ", lastestOffset=" + lastestOffset +
                ", earliestOffset=" + earliestOffset +
                '}';
    }

    public Long getEarliestOffset() {
        return earliestOffset;
    }

    public void setEarliestOffset(Long earliestOffset) {
        this.earliestOffset = earliestOffset;
    }
}
