/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.elasticsearch5;

import bigdata.java.framework.spark.pool.ConnectionFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

class ESConnectionFactory implements ConnectionFactory<TransportClient> {
    private ESConfig esConfig;
    /**
     * <p>Title: ESConnectionFactory</p>
     * <p>Description: 构造方法</p>
     *
     * @param esConfig 配置
     */
    public ESConnectionFactory(ESConfig esConfig) {
        this.esConfig = esConfig;
    }

    @Override
    public PooledObject<TransportClient> makeObject() throws Exception {
        TransportClient transportClient = this.createConnection();
        return new DefaultPooledObject<TransportClient>(transportClient);
    }

    @Override
    public void destroyObject(PooledObject<TransportClient> p)
            throws Exception {
        TransportClient transportClient = p.getObject();

        if (null != transportClient)
            transportClient.close();
    }

    @Override
    public boolean validateObject(PooledObject<TransportClient> p) {
        TransportClient transportClient = p.getObject();
        return (null != transportClient);
    }

    @Override
    public void activateObject(PooledObject<TransportClient> p)
            throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void passivateObject(PooledObject<TransportClient> p)
            throws Exception {
        // TODO Auto-generated method stub
    }

    @Override
    public TransportClient createConnection() throws Exception {
        Set<HostAndPort> nodes = esConfig.getNodes();
        Settings.Builder settings = Settings.builder();
        Properties properties = esConfig.getProperties();

        Set<Map.Entry<Object, Object>> entries = properties.entrySet();
        for (Map.Entry<Object,Object> entry : entries)
        {
            String key = entry.getKey().toString();
            String value = entry.getValue().toString();
            settings.put(key,value);
        }

        Settings build = settings.build();
        PreBuiltTransportClient client = new PreBuiltTransportClient(build);
        for (HostAndPort hostAndPort : nodes) {
            client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(hostAndPort.getHost()), hostAndPort.getPort()));
        }
        return client;
    }
}
